<?php


abstract class Model
{
  
    public function __construct()
    {
        $fields = static::getFields();

        foreach ($fields as $field) {
            if (! isset($this->$field)) {
                $this->$field = null;
            }
        }
    }

 
    public static function getFields()
    {
        return static::$fields;
    }

   
    public static function getPrimaryKey()
    {
        return static::$primaryKey;
    }

  
    public static function getTable()
    {
        return self::decamelize(get_called_class());
    }

  
    public static function decamelize($str)
    {
        return strtolower(preg_replace('/(?!^)[[:upper:]][[:lower:]]/', '$0', preg_replace('/(?!^)[[:upper:]]+/', '_$0', $str)));
    }

  
    public static function queryRecordById(PDO $pdo, $id)
    {
        $fields = implode(', ', static::getFields());
        $table = static::getTable();
        $primaryKey = static::getPrimaryKey();
        $params = ['primary_key' => $id];

        $sql = "select $fields from $table where $primaryKey = :primary_key";
        $stmt = $pdo->prepare($sql);
        $stmt->execute($params);
        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        return $stmt->fetch();
    }

   
    public static function queryRecordWithWhereClause(PDO $pdo, $where, $params = null)
    {
        $fields = implode(', ', static::getFields());
        $table = static::getTable();
        $primaryKey = static::getPrimaryKey();

        $sql = "select $fields from $table where $where";
        $stmt = $pdo->prepare($sql);
        $stmt->execute($params);
        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        return $stmt->fetch();
    }

   
    public static function queryRecordsWithWhereClause(PDO $pdo, $where, $params = null)
    {
        $fields = implode(', ', static::getFields());
        $table = static::getTable();
        $primaryKey = static::getPrimaryKey();

        $sql = "select $fields from $table where $where";
        $stmt = $pdo->prepare($sql);
        $stmt->execute($params);
        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        return $stmt->fetchAll();
    }

   
    public static function queryRecords(PDO $pdo, $search = null)
    {
        $fields = static::getFields();
        $table = static::getTable();
        $primaryKey = static::getPrimaryKey();

        $whereFields = [];
        $params = [];
        if (empty($search)) {
            $search = [];
        }

        foreach ($search as $field => $value) {
            if (in_array($field, $fields)) {
                $whereFields[] = "$field = :$field";
                $params[$field] = $value;
            }
        }

        $where = implode(', ', $whereFields);

        $fieldsSql = implode(', ', $fields);
        $sql = "select $fieldsSql from $table";
        if (! empty($where)) {
            $sql .= " where $where";
        }

        if (isset($search['sort']) && in_array($search['sort'], $fields)) {
            $sql .= ' order by ' . $search['sort'];
        }

        $stmt = $pdo->prepare($sql);
        $stmt->execute($params);
        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        return $stmt->fetchAll();
    }

   
    public static function querySql(PDO $pdo, $sql, $params = null)
    {
        $stmt = $pdo->prepare($sql);
        $stmt->execute($params);
        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        return $stmt->fetchAll();
    }

   
    public function setValues($values)
    {
        $fields = static::getFields();

        foreach ($values as $field => $value) {
            if (in_array($field, $fields)) {
                $this->$field = $value;
            }
        }
    }

  
    public function updateValues($values)
    {
        $fields = static::getFields();

        foreach ($values as $field => $value) {
            if (in_array($field, $fields) && ! empty($value)) {
                $this->$field = $value;
            }
        }
    }

   
    public function deleteRecord(PDO $pdo)
    {
        $fields = static::getFields();
        $table = static::getTable();
        $primaryKey = static::getPrimaryKey();

        if (! empty($this->$primaryKey)) {
            $sql = "delete from $table where $primaryKey = :primary_key";

            $params = [];
            $params['primary_key'] = $this->$primaryKey;

            $stmt = $pdo->prepare($sql);
            $stmt->execute($params);
        }
    }

   
    public function storeRecord(PDO $pdo)
    {
        $fields = static::getFields();
        $table = static::getTable();
        $primaryKey = static::getPrimaryKey();

       
        if (empty($this->$primaryKey)) {
            $params = [];

            foreach ($fields as $i => $field) {
                if ($field == $primaryKey) {
                    unset($fields[$i]);
                }
            }

            $fieldSql = implode(', ', $fields);

            $valueArray = [];
            foreach ($fields as $field) {
                $valueArray[] = ":$field";
                $params[$field] = $this->$field;
            }

            $valueSql = implode(', ', $valueArray);

            $sql = "insert into $table ($fieldSql) values ($valueSql)";
            $stmt = $pdo->prepare($sql);
            $stmt->execute($params);

            $this->$primaryKey = $pdo->lastInsertId();
        }
        
        else {
            $params = [];

            foreach ($fields as $i => $field) {
                if ($field == $primaryKey) {
                    unset($fields[$i]);
                }
            }

            $fieldArray = [];
            foreach ($fields as $field) {
                $fieldArray[] = "$field = :$field";
                $params[$field] = $this->$field;
            }

            $updateSql = implode(', ', $fieldArray);

            $params['primary_key'] = $this->$primaryKey;

            $sql = "update $table set $updateSql where $primaryKey = :primary_key";
            $stmt = $pdo->prepare($sql);
            $stmt->execute($params);
        }
    }
}
