<?php


class Survey extends Model
{
    public $questions = [];
    public $responses = [];
    
    protected static $primaryKey = 'survey_id';


    protected static $fields = [
        'survey_id',
        'survey_name',
    ];

  
    public function getQuestions(PDO $pdo)
    {
        $search = ['survey_id' => $this->survey_id, 'sort' => 'question_order'];
        $this->questions = Question::queryRecords($pdo, $search);
    }

   
    public function getSurveyResponses(PDO $pdo)
    {
        $driver = $pdo->getAttribute(PDO::ATTR_DRIVER_NAME);
        $groupConcatSql = 'group_concat';
        if ($driver == 'pgsql') {
            $groupConcatSql = 'string_agg';
        }

        if (! empty($this->survey_id)) {
            if (empty($this->questions)) {
                $this->getQuestions($pdo);
            }

            $questionSubSelects = [];
            foreach ($this->questions as $question) {
                $questionSubSelects[] = "(select $groupConcatSql(answer_value, ', ') from survey_answer sa
                                          where sa.survey_response_id = sr.survey_response_id and
                                          sa.question_id = :question_id_{$question->question_id}) as question_{$question->question_id}";
                $params["question_id_{$question->question_id}"] = $question->question_id;
            }
            $questionSubSelectSql = implode(', ', $questionSubSelects);
            $sql = "select sr.*, $questionSubSelectSql from survey_response sr where sr.survey_id = :survey_id";
            $params['survey_id'] = $this->survey_id;

            $stmt = $pdo->prepare($sql);
            $stmt->execute($params);
            $stmt->setFetchMode(PDO::FETCH_OBJ);
            $this->responses = $stmt->fetchAll();
        }
    }

  
    public function getSurveyResponseCounts(PDO $pdo)
    {
        foreach ($this->questions as $i => $question) {
            if (! in_array($question->question_type, ['radio', 'checkbox'])) {
                unset($this->questions[$i]);
            }
        }

        foreach ($this->questions as $i => $question) {
            $sql = 'select count(*) from survey_answer sa
                    left outer join survey_response sr on sr.survey_response_id = sa.survey_response_id
                    where sr.survey_id = :survey_id
                    and sa.question_id = :question_id
                    and sa.answer_value = :answer_value';
            $stmt = $pdo->prepare($sql);

            $question->max_answer_count = 0;

            foreach ($question->choices as $choice) {
                $params = [
                    'survey_id' => $this->survey_id,
                    'question_id' => $question->question_id,
                    'answer_value' => $choice->choice_text,
                ];
                $stmt->execute($params);
                $stmt->setFetchMode(PDO::FETCH_NUM);
                if ($row = $stmt->fetch()) {
                    $choice->answer_count = $row[0];
                    if ($choice->answer_count > $question->max_answer_count) {
                        $question->max_answer_count = $choice->answer_count;
                    }
                }
            }

            $question->choice_counts = [];
            foreach ($question->choices as $choice) {
                $question->choice_counts[] = [$choice->choice_text, $choice->answer_count];
            }
        }
    }

   
    public function getUniqueId()
    {
        if (! empty($this->survey_id)) {
            return $this->survey_id;
        } else {
            static $uniqueID;
            if (empty($uniqueID)) {
                $uniqueID = __CLASS__ . uniqid();
            }

            return $uniqueID;
        }
    }
}
