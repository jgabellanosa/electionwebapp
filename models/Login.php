<?php


class Login extends Model
{
   
    protected static $primaryKey = 'login_id';

   
    protected static $fields = [
        'login_id',
        'email',
        'password',
        'first_name',
        'last_name',
    ];

  
    public static function queryRecordByEmail(PDO $pdo, $email)
    {
        $where = 'lower(email) = :email';
        $params = ['email' => strtolower($email)];

        return parent::queryRecordWithWhereClause($pdo, $where, $params);
    }

    
    public static function cryptPassword($password)
    {
        $salt = '$6$rounds=50000$' . base64_encode(openssl_random_pseudo_bytes(10));

        return crypt($password, $salt);
    }

   
    public function validatePassword($password)
    {
        if (crypt($password, $this->password) == $this->password) {
            return true;
        } else {
            return false;
        }
    }
}
