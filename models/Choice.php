<?php


class Choice extends Model
{
    
    protected static $primaryKey = 'choice_id';

   
    protected static $fields = [
        'choice_id',
        'question_id',
        'choice_text',
        'choice_order',
    ];

   
    public function getUniqueId()
    {
        if (! empty($this->choice_id)) {
            return $this->choice_id;
        } else {
            static $uniqueID;
            if (empty($uniqueID)) {
                $uniqueID = __CLASS__ . uniqid();
            }

            return $uniqueID;
        }
    }

    
    public static function deleteChoices(PDO $pdo, $choice_ids)
    {
        if (! empty($choice_ids)) {
            $sql = 'delete from choice where choice_id in (' . implode(',', array_fill(0, count($choice_ids), '?')) . ')';
            $stmt = $pdo->prepare($sql);
            $stmt->execute(array_values($choice_ids));
        }
    }
}
