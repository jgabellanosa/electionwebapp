<?php


class SurveyResponse extends Model
{
   
    protected static $primaryKey = 'survey_response_id';

    
    protected static $fields = [
        'survey_response_id',
        'survey_id',
        'time_taken',
    ];
}
