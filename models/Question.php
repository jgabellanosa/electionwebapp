<?php


class Question extends Model
{
    public $choices = [];
    
    protected static $primaryKey = 'question_id';

    protected static $fields = [
        'question_id',
        'survey_id',
        'question_type',
        'question_text',
        'is_required',
        'question_order',
    ];

    
    public function getChoices(PDO $pdo)
    {
        $search = ['question_id' => $this->question_id, 'sort' => 'choice_order'];
        $this->choices = Choice::queryRecords($pdo, $search);
    }

    public function getUniqueId()
    {
        if (! empty($this->question_id)) {
            return $this->question_id;
        } else {
            static $uniqueID;
            if (empty($uniqueID)) {
                $uniqueID = __CLASS__ . uniqid();
            }

            return $uniqueID;
        }
    }

   
    public static function deleteQuestions(PDO $pdo, $question_ids)
    {
        if (! empty($question_ids)) {
            $sql = 'delete from question where question_id in (' . implode(',', array_fill(0, count($question_ids), '?')) . ')';
            $stmt = $pdo->prepare($sql);
            $stmt->execute(array_values($question_ids));
        }
    }
}
