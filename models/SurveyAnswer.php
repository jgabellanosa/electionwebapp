<?php


class SurveyAnswer extends Model
{
   
    protected static $primaryKey = 'survey_answer_id';

   
    protected static $fields = [
        'survey_answer_id',
        'survey_response_id',
        'question_id',
        'answer_value',
    ];
}
