<?php


class SurveyEditController extends Controller
{
    
    protected function handleRequest(&$request)
    {
        $user = $this->getUserSession();
        $this->assign('user', $user);

        if (isset($request['action'])) {
            $this->handleAction($request);
        }

        $survey = $this->getSurvey($request);
        $this->assign('survey', $survey);

        if (isset($request['status']) && $request['status'] == 'success') {
            $this->assign('statusMessage', 'Survey updated successfully');
        }
    }

  
    protected function handleAction(&$request)
    {
        switch ($request['action']) {
            case 'get_survey':
                $this->getSurvey($request['survey_id']);
                break;

            case 'edit_survey':
                $this->editSurvey($request);
                break;

            case 'delete_survey':
                $this->deleteSurvey($request);
                break;
        }
    }

    
    protected function getSurvey(&$request)
    {
        if (! empty($request['survey_id'])) {
            $survey = Survey::queryRecordById($this->pdo, $request['survey_id']);
            if (! $survey) {
                throw new Exception('Survey ID not found in database');
            }
            
            $survey->existing_question_ids = [];
            $survey->existing_choice_ids = [];

            $survey->getQuestions($this->pdo);
            foreach ($survey->questions as $question) {
                $survey->existing_question_ids[] = $question->question_id;
                $question->getChoices($this->pdo);

                foreach ($question->choices as $choice) {
                    $survey->existing_choice_ids[] = $choice->choice_id;
                }
            }
        } else {
            $survey = new Survey;
            $survey->questions = [];

            
            $question = new Question;
            $question->question_type = 'checkbox';
            $question->choices = [];

            
            $choice = new Choice;
            $question->choices[] = $choice;

            $survey->questions[] = $question;
        }

        return $survey;
    }

    
    protected function setSurveyValues(Survey $survey, &$request)
    {
        $survey->updateValues($request);

        $this->setSurveyQuestions($survey, $request);
    }

    
    protected function setSurveyQuestions(Survey $survey, &$request)
    {
        if (! empty($request['question_type'])) {
            $survey->questions = [];
            $questionOrder = 1;
            foreach ($request['question_type'] as $questionID => $questionType) {
                $question = new Question;
                if (is_numeric($questionID)) {
                    $question->question_id = $questionID;
                }
                $question->question_type = $questionType;
                $question->question_text = $request['question_text'][$questionID];
                $question->is_required = isset($request['is_required'][$questionID]) ? 1 : 0;
                $question->question_order = $questionOrder++;

                $this->setQuestionChoices($question, $questionID, $request);
                $survey->questions[] = $question;
            }
        }
    }

    
    protected function setQuestionChoices(Question $question, $questionID, &$request)
    {
        if (in_array($question->question_type, ['radio', 'checkbox']) && isset($request['choice_text'][$questionID])) {
            $question->choices = [];
            $choiceOrder = 1;
            foreach ($request['choice_text'][$questionID] as $choiceID => $choiceText) {
                $choice = new Choice;
                if (is_numeric($choiceID)) {
                    $choice->choice_id = $choiceID;
                }
                $choice->choice_text = $choiceText;
                $choice->choice_order = $choiceOrder++;
                $question->choices[] = $choice;
            }
        }
    }

    
    protected function storeSurvey(Survey $survey)
    {
        
        $stored_question_ids = [];
        $stored_choice_ids = [];

        $survey->storeRecord($this->pdo);

        foreach ($survey->questions as $question) {
            $question->survey_id = $survey->survey_id;
            $question->storeRecord($this->pdo);
            $stored_question_ids[] = $question->question_id;

            foreach ($question->choices as $choice) {
                $choice->question_id = $question->question_id;
                $choice->storeRecord($this->pdo);
                $stored_choice_ids[] = $choice->choice_id;
            }
        }

      
        if (! empty($survey->existing_choice_ids)) {
            $deleted_choice_ids = array_diff($survey->existing_choice_ids, $stored_choice_ids);
            Choice::deleteChoices($this->pdo, $deleted_choice_ids);
        }

       
        if (! empty($survey->existing_question_ids)) {
            $deleted_question_ids = array_diff($survey->existing_question_ids, $stored_question_ids);
            Question::deleteQuestions($this->pdo, $deleted_question_ids);
        }
    }

    
    protected function editSurvey(&$request)
    {
        $this->pdo->beginTransaction();

        
        $survey = $this->getSurvey($request);

        
        $this->setSurveyValues($survey, $request);

       
        $this->storeSurvey($survey);

        $this->pdo->commit();

        $this->redirect('survey_edit.php?survey_id=' . $survey->survey_id . '&status=success');
    }

    
    protected function deleteSurvey(&$request)
    {
        if (! empty($request['survey_id'])) {
            $this->pdo->beginTransaction();

            $survey = Survey::queryRecordById($this->pdo, $request['survey_id']);
            $survey->deleteRecord($this->pdo);

            $this->pdo->commit();

            $this->redirect('surveys.php?status=deleted');
        }
    }
}
