<?php


class LogoutController extends Controller
{
    
    protected function handleRequest(&$request)
    {
        $user = $this->getUserSession();
        $this->assign('user', $user);

        $this->destroyUserSession();

        $this->redirect('login.php');
    }

   
    protected function destroyUserSession()
    {
        
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 3600, $params['path'], $params['domain']);

      
        session_destroy();
        unset($this->user);
    }
}
