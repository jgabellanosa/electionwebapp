<?php


class LoginController extends Controller
{
    
    protected function handleRequest(&$request)
    {
        if (isset($_POST['email']) && isset($_POST['password']) && empty($_POST['email']) && empty($_POST['password'])) {
            throw new Exception('Please enter your e-mail and password.');
        }
        if (! empty($_POST['email']) && empty($_POST['password'])) {
            throw new Exception('Please enter your password.');
        }
        if (isset($_POST['email']) && isset($_POST['password'])) {
            $success = false;

            // Query the database by email
            $login = Login::queryRecordByEmail($this->pdo, $_POST['email']);

            if ($login) {
                $success = $this->authenticateLogin($login, $_POST['password']);
            }

            if ($success) {
                $this->createUserSession($login);

                $this->redirect('index.php');
            } else {
                throw new Exception('Invalid e-mail/password. Please try again.');
            }
        }
    }

    protected function createUserSession(Login $login)
    {
        $this->startSession();

        $_SESSION['login'] = $login;
    }

    
    protected function authenticateLogin(Login $login, $password)
    {
        return $login->validatePassword($password);
    }
}
