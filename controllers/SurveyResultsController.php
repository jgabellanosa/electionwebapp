<?php


class SurveyResultsController extends Controller
{
  
    protected function handleRequest(&$request)
    {
        $user = $this->getUserSession();
        $this->assign('user', $user);

        $survey = $this->getSurvey($request);
        $responses = $survey->getSurveyResponses($this->pdo);
        $this->assign('survey', $survey);

        if (isset($request['action'])) {
            $this->handleAction($request, $survey);
        }
    }

   
    protected function handleAction(&$request, Survey $survey)
    {
        switch ($request['action']) {
            case 'download_csv':
                $this->downloadCsv($survey);
                break;
        }
    }

   
    protected function downloadCsv(Survey $survey)
    {
        header('Pragma: private');
        header('Cache-control: private, must-revalidate');
        header('Content-type: text/csv');
        $csvFileName = preg_replace('/[^A-Za-z0-9_-]/', '', str_replace(' ', '_', $survey->survey_name));
        header('Content-Disposition: attachment; filename=' . $csvFileName . '.csv');

        $fp = fopen('php://output', 'w');

        $headers = [];
        foreach ($survey->questions as $question) {
            $headers[] = iconv('UTF-8', 'WINDOWS-1252', $question->question_text);
        }

        fputcsv($fp, $headers);

        foreach ($survey->responses as $response) {
            $row = [];
            foreach ($survey->questions as $question) {
                $field = 'question_' . $question->question_id;
                $row[] = iconv('UTF-8', 'WINDOWS-1252', $response->$field);
            }
            fputcsv($fp, $row);
        }

        fclose($fp);
        exit;
    }

   
    protected function getSurvey(&$request)
    {
        if (! empty($request['survey_id'])) {
            $survey = Survey::queryRecordById($this->pdo, $request['survey_id']);
            if (! $survey) {
                throw new Exception('Survey ID not found in database');
            }
        } else {
            throw new Exception('Survey ID must be specified');
        }

        return $survey;
    }
}
