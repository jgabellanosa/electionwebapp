<?php


class SurveyThankYouController extends Controller
{
  
    protected function handleRequest(&$request)
    {
        $survey = $this->getSurvey($request);
        $this->assign('survey', $survey);
    }

  
    protected function getSurvey(&$request)
    {
        if (! empty($request['survey_id'])) {
            $survey = Survey::queryRecordById($this->pdo, $request['survey_id']);
            if (! $survey) {
                throw new Exception('Survey ID not found in database');
            }
        } else {
            throw new Exception('Survey ID must be specified');
        }

        return $survey;
    }
}
