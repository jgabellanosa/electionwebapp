<?php


class SurveysController extends Controller
{
  
    protected function handleRequest(&$request)
    {
        $user = $this->getUserSession();
        $this->assign('user', $user);

        $surveys = Survey::queryRecords($this->pdo, ['sort' => 'survey_name']);
        $this->assign('surveys', $surveys);

        if (isset($request['status']) && $request['status'] == 'deleted') {
            $this->assign('statusMessage', 'Survey deleted successfully');
        }
    }
}
