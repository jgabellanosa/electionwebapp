<?php

class UsersController extends Controller
{
 
    protected function handleRequest(&$request)
    {
        $user = $this->getUserSession();
        $this->assign('user', $user);

        $loginFields = Login::getFields();
        $this->assign('loginFields', $loginFields);

        $users = Login::queryRecords($this->pdo, ['sort' => 'first_name']);
        $this->assign('users', $users);
    }
}
