<?php


class SurveyChartsController extends Controller
{
  
    protected function handleRequest(&$request)
    {
        $user = $this->getUserSession();
        $this->assign('user', $user);

        $survey = $this->getSurvey($request);
        $survey->getSurveyResponseCounts($this->pdo);
        $this->assign('survey', $survey);
    }

    protected function getSurvey(&$request)
    {
        if (! empty($request['survey_id'])) {
            $survey = Survey::queryRecordById($this->pdo, $request['survey_id']);
            if (! $survey) {
                throw new Exception('Survey ID not found in database');
            }
            $survey->getQuestions($this->pdo);
            foreach ($survey->questions as $question) {
                $question->getChoices($this->pdo);
            }
        } else {
            throw new Exception('Survey ID must be specified');
        }

        return $survey;
    }
}
