<?php


class UserEditController extends Controller
{
   
    protected function handleRequest(&$request)
    {
        $user = $this->getUserSession();
        $this->assign('user', $user);

        if (isset($request['action'])) {
            $this->handleAction($request);
        }

        $this->redirect('users.php');
    }

  
    protected function handleAction(&$request)
    {
        switch ($request['action']) {
            case 'get_user':
                $this->getUser($request['login_id']);
                break;

            case 'edit_user':
                $this->editUser($request);
                break;

            case 'delete_user':
                $this->deleteUser($request);
                break;
        }
    }

  
    protected function getUser($loginID)
    {
        $login = Login::queryRecordById($this->pdo, $loginID);
        unset($login->password);
        die(json_encode(['login' => $login]));
    }

  
    protected function editUser(&$request)
    {
        if (! empty($request['login_id'])) {
            $login = Login::queryRecordById($this->pdo, $request['login_id']);
        } else {
            $login = new Login;
        }

        $login->updateValues($request);

        if (! empty($request['password'])) {
            $login->password = Login::cryptPassword($request['password']);
        }

        $this->pdo->beginTransaction();

        $login->storeRecord($this->pdo);

        $this->pdo->commit();
    }

 
    protected function deleteUser(&$request)
    {
        if (! empty($request['login_id'])) {
            $this->pdo->beginTransaction();

            $login = Login::queryRecordById($this->pdo, $request['login_id']);
            $login->deleteRecord($this->pdo);

            $this->pdo->commit();
        }
    }
}
