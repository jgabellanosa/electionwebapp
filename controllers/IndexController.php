<?php


class IndexController extends Controller
{
   
    protected function handleRequest(&$request)
    {
        $user = $this->getUserSession();
        $this->assign('user', $user);
    }
}
