<?php


$projectDir = dirname(dirname(__FILE__));
$classDirectories = ['models', 'controllers'];
$classPaths = [];
foreach ($classDirectories as $dir) {
    $classPaths[] = $projectDir . DIRECTORY_SEPARATOR . $dir;
}
$classPaths[] = get_include_path();
$includePath = implode(PATH_SEPARATOR, $classPaths);
set_include_path($includePath);


spl_autoload_register(['Controller', 'autoload']);


abstract class Controller
{
    const DSN_CONFIG_FILE = 'config/Database.ini';
    const DATABASE_SCHEMA_FILE = 'data/schema.sql';
    const DATABASE_SCHEMA_FILE_MYSQL = 'data/schema_mysql.sql';
    const DATABASE_SCHEMA_FILE_POSTGRESQL = 'data/schema_postgresql.sql';
    const INITIAL_DATA_FILE = 'data/initial_data.sql';
    const SESSION_NAME = 'SURVEYFORMAPP';
    const RUNTIME_EXCEPTION_VIEW = 'runtime_exception.php';

    protected $config;
    protected $dsn;
    protected $driver = 'sqlite';
    protected $pdo;
    protected $viewVariables = [];

    
    public static function autoload($class)
    {
        require $class . '.php';

        return true;
    }

    
    public function display()
    {
       
        header('Content-type: text/html; charset=utf-8');

        try {
            
            $this->checkDependencies();

           
            $this->openDatabase();

           
            $this->handleRequest($_REQUEST);

           
            $viewFilename = $this->getViewFilename();

          
            $this->displayView($viewFilename);
        } catch (RuntimeException $e) {
            $this->assign('statusMessage', $e->getMessage());
            $this->displayView(self::RUNTIME_EXCEPTION_VIEW);
        } catch (Exception $e) {
           
            $this->handleError($e);

            
            $viewFilename = $this->getViewFilename();

           
            if ($viewFilename) {
                $this->displayView($viewFilename);
            } else {
                die($e->getMessage());
            }
        }
    }

    
    protected function getViewFilename()
    {
        return basename($_SERVER['SCRIPT_NAME']);
    }

    
    abstract protected function handleRequest(&$request);

    
    protected function handleError(Exception $e)
    {
        $this->assign('statusMessage', $e->getMessage());
    }

    protected function assign($name, $value)
    {
        $this->viewVariables[$name] = $value;
    }

    
    protected function displayView($viewFilename)
    {
        if (! file_exists($viewFilename)) {
            throw new RuntimeException("Filename does not exist: $viewFilename");
        }

        
        extract($this->viewVariables);

       
        require realpath(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $viewFilename);
    }

   
    protected function startSession()
    {
        session_name(self::SESSION_NAME);

        session_start();
    }

    
    protected function getUserSession()
    {
        $this->startSession();

        if (! isset($_SESSION['login'])) {
            $this->redirect('login.php');
        }

        return $_SESSION['login'];
    }

    
    protected function redirect($url)
    {
        
        if (session_id() != '') {
            session_write_close();
        }

        header("Location: $url");
        exit;
    }

  
    protected function checkDependencies()
    {
        $missing = [];

        if (! extension_loaded('openssl')) {
            $missing[] = 'openssl';
        }

        if (! extension_loaded('pdo')) {
            $missing[] = 'PDO';
        }

        if (! extension_loaded('pdo_sqlite')) {
            $missing[] = 'pdo_sqlite';
        } else {
           
            if (extension_loaded('sqlite3')) {
                $versionArray = sqlite3::version();
                $versionString = $versionArray['versionString'];
            } else {
               
                $pdo = new PDO('sqlite::memory:');
                $versionString = $pdo->query('select sqlite_version()')->fetch()[0];
            }

            if (version_compare($versionString, '3.6.19', '<')) {
                $missing[] = 'sqlite3 version 3.6.19 or higher';
            }
        }

        if (! empty($missing)) {
            throw new RuntimeException("The following PHP extensions are required:\n\n" . implode("\n", $missing));
        }
    }

    
    protected function openDatabase()
    {
        if (! file_exists(self::DSN_CONFIG_FILE)) {
            throw new RuntimeException('Database config file not found: ' . self::DSN_CONFIG_FILE);
        }
        $databaseConfig = parse_ini_file(self::DSN_CONFIG_FILE);
        if (! isset($databaseConfig['dsn'])) {
            throw new RuntimeException("Database config parameter 'dsn' not found in config file: " . self::DSN_CONFIG_FILE);
        }

        $username = null;
        $password = null;

        if (preg_match('/^(mysql|pgsql)/', $databaseConfig['dsn'], $matches)) {
            $this->driver = $matches[1];
            if (! isset($databaseConfig['username'])) {
                throw new RuntimeException("Database config parameter 'username' not found in config file: " . self::DSN_CONFIG_FILE);
            }
            if (! isset($databaseConfig['password'])) {
                throw new RuntimeException("Database config parameter 'password' not found in config file: " . self::DSN_CONFIG_FILE);
            }
            $username = $databaseConfig['username'];
            $password = $databaseConfig['password'];
        } else {
            if (! isset($databaseConfig['filename'])) {
                throw new RuntimeException("Database config parameter 'filename' not found in config file: " . self::DSN_CONFIG_FILE);
            }
            if (! is_writable(dirname($databaseConfig['filename']))) {
                throw new RuntimeException('Data directory not writable by web server: ' . dirname($databaseConfig['filename']) . '/');
            }
            if (! is_writable(dirname($databaseConfig['filename'])) || (file_exists($databaseConfig['filename']) && ! is_writable($databaseConfig['filename']))) {
                throw new RuntimeException('Database file not writable by web server: ' . $databaseConfig['filename']);
            }
        }
        try {
            $this->pdo = new PDO($databaseConfig['dsn'], $username, $password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            if ($this->driver == 'sqlite') {
                $this->pdo->exec('PRAGMA foreign_keys = ON;');
            }

            if (! $this->databaseTablesCreated()) {
                $this->createDatabaseTables();
            }
        } catch (PDOException $e) {
            throw new RuntimeException('PDOException: ' . $e->getMessage());
        }
    }

    
    protected function createDatabaseTables()
    {
        $schemaFile = self::DATABASE_SCHEMA_FILE;
        if ($this->driver == 'mysql') {
            $schemaFile = self::DATABASE_SCHEMA_FILE_MYSQL;
        } elseif ($this->driver == 'pgsql') {
            $schemaFile = self::DATABASE_SCHEMA_FILE_POSTGRESQL;
        }

        if (! file_exists($schemaFile)) {
            throw new RuntimeException('Database schema file not found: ' . $schemaFile);
        }
       
        $sql = file_get_contents($schemaFile);
        $this->pdo->exec($sql);

       
        $sql = file_get_contents(self::INITIAL_DATA_FILE);
        $this->pdo->exec($sql);
    }

   
    protected function databaseTablesCreated()
    {
        if ($this->driver == 'mysql') {
            $sql = "show tables like 'login'";
        } elseif ($this->driver == 'pgsql') {
            $sql = "select table_name from information_schema.tables where table_schema='public' and table_name='login'";
        } else {
            $sql = "select count(*) from sqlite_master where type='table' and name='login'";
        }
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_NUM);
        if ($row = $stmt->fetch()) {
            if ($this->driver == 'mysql') {
                
                if ($row[0] == 'login') {
                    return true;
                }
            } elseif ($this->driver == 'pgsql') {
               
                if ($row[0] == 'login') {
                    return true;
                }
            } else {
               
                if ($row[0] == 1) {
                    return true;
                }
            }
        }

        return false;
    }
}
